package com.asesoriabancaria.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Cliente")
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, name = "cod_cliente")
	private int codCliente;
	@Column(name = "nom_cliente")
	private String nomCliente;
	@Column(name = "ape_cliente")
	private String apeCliente;
	@Column(name = "dni_cliente")
	private String dniCliente;
	@Column(name = "fono_cliente")
	private int fonoCliente;
	@Column(name = "email_cliente")
	private String emailCliente;
	@Column(name = "pass_cliente")
	private String passCliente;

	public Cliente() {
	}

	public int getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(int codCliente) {
		this.codCliente = codCliente;
	}

	public String getNomCliente() {
		return nomCliente;
	}

	public void setNomCliente(String nomCliente) {
		this.nomCliente = nomCliente;
	}

	public String getApeCliente() {
		return apeCliente;
	}

	public void setApeCliente(String apeCliente) {
		this.apeCliente = apeCliente;
	}

	public String getDniCliente() {
		return dniCliente;
	}

	public void setDniCliente(String dniCliente) {
		this.dniCliente = dniCliente;
	}

	public int getFonoCliente() {
		return fonoCliente;
	}

	public void setFonoCliente(int fonoCliente) {
		this.fonoCliente = fonoCliente;
	}

	public String getEmailCliente() {
		return emailCliente;
	}

	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}

	public String getPassCliente() {
		return passCliente;
	}

	public void setPassCliente(String passCliente) {
		this.passCliente = passCliente;
	}

}
