package com.asesoriabancaria.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Respuesta")
public class Respuesta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, name = "cod_respuesta")
	private int codRespuesta;
	@Column(name = "des_respuesta")
	private String desRespuesta;

	@Column(name = "fecha_respuesta")
	private String fechaRespuesta;

	@OneToOne
	@JoinColumn(name = "cod_consulta", nullable = false, foreignKey = @ForeignKey(name = "FK_respuesta_consulta"))
	private Consulta consulta;

	public Respuesta() {

	}

	public String getFechaRespuesta() {
		return fechaRespuesta;
	}

	public void setFechaRespuesta(String fechaRespuesta) {
		this.fechaRespuesta = fechaRespuesta;
	}

	public int getCodRespuesta() {
		return codRespuesta;
	}

	public void setCodRespuesta(int codRespuesta) {
		this.codRespuesta = codRespuesta;
	}

	public String getDesRespuesta() {
		return desRespuesta;
	}

	public void setDesRespuesta(String desRespuesta) {
		this.desRespuesta = desRespuesta;
	}

	public Consulta getConsulta() {
		return consulta;
	}

	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}

}
