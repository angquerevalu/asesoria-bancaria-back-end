package com.asesoriabancaria.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Categoria")
public class Categoria {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, name = "cod_categoria")
	private int codCategoria;
	@Column(name = "des_categoria")
	private String desCategoria;
	@Column(name = "precio_categoria")
	private Double precioCategoria;

	public Categoria() {

	}

	public int getCodCategoria() {
		return codCategoria;
	}

	public void setCodCategoria(int codCategoria) {
		this.codCategoria = codCategoria;
	}

	public String getDesCategoria() {
		return desCategoria;
	}

	public void setDesCategoria(String desCategoria) {
		this.desCategoria = desCategoria;
	}

	public Double getPrecioCategoria() {
		return precioCategoria;
	}

	public void setPrecioCategoria(Double precioCategoria) {
		this.precioCategoria = precioCategoria;
	}

}
