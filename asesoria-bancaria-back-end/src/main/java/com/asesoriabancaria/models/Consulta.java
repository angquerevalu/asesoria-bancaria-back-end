package com.asesoriabancaria.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Consulta")
public class Consulta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, name = "cod_consulta")
	private int codConsulta;
	@Column(name = "des_consulta")
	private String desConsulta;
	@Column(name = "fecha_consulta")
	private String fechaConsulta;
	@ManyToOne
	@JoinColumn(name = "cod_estado", nullable = false, foreignKey = @ForeignKey(name = "FK_consulta_estado"))
	private Estado estado;
	@ManyToOne
	@JoinColumn(name = "cod_cliente", nullable = false, foreignKey = @ForeignKey(name = "FK_consulta_cliente"))
	private Cliente cliente;
	@ManyToOne
	@JoinColumn(name = "cod_categoria", nullable = false, foreignKey = @ForeignKey(name = "FK_consulta_categoria"))
	private Categoria categoria;

	public Consulta() {

	}

	public String getDesConsulta() {
		return desConsulta;
	}

	public void setDesConsulta(String desConsulta) {
		this.desConsulta = desConsulta;
	}

	public int getCodConsulta() {
		return codConsulta;
	}

	public void setCodConsulta(int codConsulta) {
		this.codConsulta = codConsulta;
	}

	public String getFechaConsulta() {
		return fechaConsulta;
	}

	public void setFechaConsulta(String fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

}
