package com.asesoriabancaria.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Pago")
public class Pago {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, name = "cod_pago")
	private int codPago;

	@Column(name = "month_exp")
	private String monthExp;

	@Column(name = "year_exp")
	private String yearExp;

	@Column(name = "ccv")
	private String ccv;

	@Column(name = "nom_titular")
	private String nomTitular;

	@ManyToOne
	@JoinColumn(name = "cod_cliente", nullable = false, foreignKey = @ForeignKey(name = "FK_pago_cliente"))
	private Cliente cliente;

	public Pago() {

	}

	public int getCodPago() {
		return codPago;
	}

	public void setCodPago(int codPago) {
		this.codPago = codPago;
	}

	public String getMonthExp() {
		return monthExp;
	}

	public void setMonthExp(String monthExp) {
		this.monthExp = monthExp;
	}

	public String getYearExp() {
		return yearExp;
	}

	public void setYearExp(String yearExp) {
		this.yearExp = yearExp;
	}

	public String getCcv() {
		return ccv;
	}

	public void setCcv(String ccv) {
		this.ccv = ccv;
	}

	public String getNomTitular() {
		return nomTitular;
	}

	public void setNomTitular(String nomTitular) {
		this.nomTitular = nomTitular;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
