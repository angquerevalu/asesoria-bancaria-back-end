package com.asesoriabancaria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asesoriabancaria.models.Cliente;
import com.asesoriabancaria.service.ClienteService;

@RestController
@RequestMapping("api/v1/cliente")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@GetMapping("/listar")
	public List<Cliente> obtenerClientes() {
		return clienteService.obtenerClientes();
	}

	@PostMapping("/registrar")
	public Cliente guardarCliente(@RequestBody Cliente cliente) {
		return clienteService.guardarCliente(cliente);
	}

	@PostMapping("/login")
	public ResponseEntity<Cliente> login(@RequestBody Cliente c) {
		Cliente cliente = clienteService.login(c.getEmailCliente(), c.getPassCliente());
		return new ResponseEntity<>(cliente, HttpStatus.OK);
	}

	@DeleteMapping("/delete")
	public void eliminarRegistros() {
		clienteService.eliminarRegistros();
	}

	@PutMapping("/update")
	public ResponseEntity<Cliente> modificar(@RequestBody Cliente c) {
		Cliente cliente = clienteService.update(c);
		return new ResponseEntity<>(cliente, HttpStatus.OK);
	}
}
