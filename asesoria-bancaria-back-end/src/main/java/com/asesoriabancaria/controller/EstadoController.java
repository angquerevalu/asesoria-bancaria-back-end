package com.asesoriabancaria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asesoriabancaria.models.Estado;
import com.asesoriabancaria.service.EstadoService;

@RestController
@RequestMapping("api/v1/estado")
public class EstadoController {

	@Autowired
	EstadoService estadoService;

	@GetMapping("/listar")
	public ResponseEntity<List<Estado>> listar() {
		List<Estado> estados;
		estados = estadoService.obtenerEstados();
		return new ResponseEntity<>(estados, HttpStatus.OK);
	}

	@PostMapping("/save")
	public ResponseEntity<Estado> saveConsulta(@RequestBody Estado e) {
		Estado estado = estadoService.addEstado(e);
		return new ResponseEntity<>(estado, HttpStatus.CREATED);
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Estado obj = estadoService.findById(id);
		if (obj == null) {
			throw new Exception("ID NO ENCONTRADO " + id);
		}
		estadoService.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PutMapping("/update")
	public ResponseEntity<Estado> modificar(@RequestBody Estado e) {
		Estado obj = estadoService.updateEstado(e);
		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@GetMapping("/siguienteCodigo")
	public int siguienteCodigo() {
		int numero;
		try {
			numero = estadoService.siguienteCodigo();
		} catch (Exception e) {
			numero = 0;
		}
		return numero;
	}
}
