package com.asesoriabancaria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asesoriabancaria.models.Categoria;
import com.asesoriabancaria.service.CategoriaService;

@RestController
@RequestMapping("api/v1/categoria")
public class CategoriaController {
	@Autowired
	private CategoriaService categoriaService;

	@GetMapping("/listar")
	public ResponseEntity<List<Categoria>> listarResumen() {
		List<Categoria> lstCate;
		lstCate = categoriaService.findCategoria();
		return new ResponseEntity<>(lstCate, HttpStatus.OK);
	}

	@PostMapping("/save")
	public ResponseEntity<Categoria> saveConsulta(@RequestBody Categoria c) {
		Categoria cate = categoriaService.addCategoria(c);
		return new ResponseEntity<>(cate, HttpStatus.CREATED);
	}

	@PutMapping("/update")
	public ResponseEntity<Categoria> modificar(@RequestBody Categoria c) {
		Categoria obj = categoriaService.updateCategoria(c);
		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Categoria obj = categoriaService.findById(id);
		if (obj == null) {
			throw new Exception("ID NO ENCONTRADO " + id);
		}
		categoriaService.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Categoria> listarPorId(@PathVariable("id") Integer id) throws Exception {
		Categoria obj = categoriaService.findById(id);

		if (obj == null) {
			throw new Exception("ID NO ENCONTRADO " + id);
		}

		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@GetMapping("/siguienteCodigo")
	public int siguienteCodigo() {
		int numero;
		try {
			numero = categoriaService.siguienteCodigo();
		} catch (Exception e) {
			numero = 0;
		}
		return numero;
	}

}
