package com.asesoriabancaria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asesoriabancaria.dto.ConsultaDto;
import com.asesoriabancaria.models.Consulta;
import com.asesoriabancaria.service.ConsultaService;

@RestController
@RequestMapping("api/v1/consulta")
public class ConsultaController {
	@Autowired
	private ConsultaService consultaService;

	@GetMapping("/listarConsulta")
	public ResponseEntity<List<ConsultaDto>> listarCosultas() {
		List<ConsultaDto> consultas;
		consultas = consultaService.listConsulta();
		return new ResponseEntity<>(consultas, HttpStatus.OK);
	}

	@GetMapping("/listar")
	public ResponseEntity<List<Consulta>> listar() {
		List<Consulta> consultas;
		consultas = consultaService.findConsulta();
		return new ResponseEntity<>(consultas, HttpStatus.OK);
	}

	@PostMapping("/save")
	public ResponseEntity<Consulta> saveConsulta(@RequestBody Consulta con) {
		Consulta consulta = consultaService.addConsulta(con);
		return new ResponseEntity<>(consulta, HttpStatus.CREATED);
	}

	@PutMapping("/update")
	public ResponseEntity<Consulta> modificar(@RequestBody Consulta c) {
		Consulta obj = consultaService.updateConsulta(c);
		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Consulta obj = consultaService.findById(id);
		if (obj == null) {
			throw new Exception("ID NO ENCONTRADO " + id);
		}
		consultaService.eliminar(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Consulta> listarPorId(@PathVariable("id") Integer id) throws Exception {
		Consulta obj = consultaService.findById(id);

		if (obj == null) {
			throw new Exception("ID NO ENCONTRADO " + id);
		}

		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@GetMapping("/listar/{cod_cliente}")
	public ResponseEntity<List<Consulta>> listarPorCodCliente(@PathVariable("cod_cliente") Integer codCliente)
			throws Exception {
		List<Consulta> obj = consultaService.listConsultaxCliente(codCliente);

		if (obj == null) {
			throw new Exception("ID NO ENCONTRADO " + codCliente);
		}

		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@GetMapping("/siguienteCodigo")
	public int siguienteCodigo() {
		int numero;
		try {
			numero = consultaService.siguienteCodigo();
		} catch (Exception e) {
			numero = 0;
		}
		return numero;
	}

}
