package com.asesoriabancaria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asesoriabancaria.models.Pago;
import com.asesoriabancaria.service.PagoService;

@RestController
@RequestMapping("api/v1/pago")
public class PagoController {

	@Autowired
	PagoService pagoService;

	@PostMapping("/save")
	public ResponseEntity<Pago> addPago(@RequestBody Pago p) {
		Pago pago = pagoService.addPago(p);
		return new ResponseEntity<>(pago, HttpStatus.CREATED);
	}
}
