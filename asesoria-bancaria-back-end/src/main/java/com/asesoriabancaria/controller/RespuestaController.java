package com.asesoriabancaria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asesoriabancaria.models.Respuesta;
import com.asesoriabancaria.service.RespuestaService;

@RestController
@RequestMapping("api/v1/respuesta")
public class RespuestaController {

	@Autowired
	RespuestaService respuestaService;

	@PostMapping("/save")
	public ResponseEntity<Respuesta> saveConsulta(@RequestBody Respuesta res) {
		Respuesta respuesta = respuestaService.addRespuesta(res);
		return new ResponseEntity<>(respuesta, HttpStatus.CREATED);
	}

	@GetMapping("/listar/{cod_consulta}")
	public ResponseEntity<Respuesta> respuestaxConsulta(@PathVariable("cod_consulta") Integer codConsulta)
			throws Exception {
		Respuesta obj = respuestaService.existeRespuesta(codConsulta);

		if (obj == null) {
			throw new Exception("ID NO ENCONTRADO " + codConsulta);
		}

		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

}
