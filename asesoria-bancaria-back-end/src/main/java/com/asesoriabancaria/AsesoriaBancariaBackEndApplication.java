package com.asesoriabancaria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsesoriaBancariaBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsesoriaBancariaBackEndApplication.class, args);
	}

}
