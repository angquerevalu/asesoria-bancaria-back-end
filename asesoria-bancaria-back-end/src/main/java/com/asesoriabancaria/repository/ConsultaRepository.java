package com.asesoriabancaria.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.asesoriabancaria.models.Consulta;

@Repository
public interface ConsultaRepository extends JpaRepository<Consulta, Integer> {
	@Query(value = " select 	con.des_consulta as desCategoria, " + "		cat.des_categoria as categoria, "
			+ "		cat.precio_categoria as precio, " + "		est.des_estado as estado " + " from consulta con "
			+ " inner join categoria cat " + " on con.cod_categoria=cat.cod_categoria " + " inner join estado est "
			+ " on con.cod_estado = est.cod_estado;", nativeQuery = true)
	List<Object[]> listConsulta();

	@Query("Select c from Consulta c inner join Cliente cli on c.cliente.codCliente = cli.codCliente where cli.codCliente =?1")
	public List<Consulta> listarConsultaxCliente(int codCliente);

	@Query(value = "Select c.cod_consulta from consulta c order by c.cod_consulta desc limit 0,1", nativeQuery = true)
	public int siguienteCodigo();
}
