package com.asesoriabancaria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.asesoriabancaria.models.Respuesta;

@Repository
public interface RespuestaRepository extends JpaRepository<Respuesta, Integer> {

	@Query("Select r from Respuesta r inner join Consulta con on r.consulta.codConsulta = con.codConsulta where con.codConsulta =?1")
	public Respuesta respuestaxConsulta(int codConsulta);
}
