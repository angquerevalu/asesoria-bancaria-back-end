package com.asesoriabancaria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.asesoriabancaria.models.Estado;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Integer> {
	@Query(value = "Select c.cod_estado from estado c order by c.cod_estado desc limit 0,1", nativeQuery = true)
	public int siguienteCodigo();
}
