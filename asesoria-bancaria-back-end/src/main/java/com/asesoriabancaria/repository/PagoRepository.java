package com.asesoriabancaria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asesoriabancaria.models.Pago;

@Repository
public interface PagoRepository extends JpaRepository<Pago, Integer> {

}
