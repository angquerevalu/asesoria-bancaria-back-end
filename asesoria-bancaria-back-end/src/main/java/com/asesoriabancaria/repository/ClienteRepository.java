package com.asesoriabancaria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.asesoriabancaria.models.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
	@Query("Select c from Cliente c where c.emailCliente =?1 and c.passCliente =?2")
	public Cliente login(String email, String pass);
}
