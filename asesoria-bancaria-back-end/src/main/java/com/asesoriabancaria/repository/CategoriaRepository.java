package com.asesoriabancaria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.asesoriabancaria.models.Categoria;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Integer> {
	@Query(value = "Select c.cod_categoria from categoria c order by c.cod_categoria desc limit 0,1", nativeQuery = true)
	public int siguienteCodigo();
}
