package com.asesoriabancaria.service;

import java.util.List;

import com.asesoriabancaria.models.Categoria;

public interface CategoriaService {

	void eliminar(int cod);

	List<Categoria> findCategoria();

	Categoria addCategoria(Categoria categoria);

	Categoria updateCategoria(Categoria categoria);

	Categoria findById(int cod);

	int siguienteCodigo();
}
