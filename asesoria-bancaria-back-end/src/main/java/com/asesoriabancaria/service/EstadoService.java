package com.asesoriabancaria.service;

import java.util.List;

import com.asesoriabancaria.models.Estado;

public interface EstadoService {

	public List<Estado> obtenerEstados();

	public Estado addEstado(Estado e);

	Estado findById(int cod);

	void eliminar(int cod);

	Estado updateEstado(Estado estado);

	int siguienteCodigo();
}
