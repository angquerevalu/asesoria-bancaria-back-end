package com.asesoriabancaria.service;

import java.util.List;

import com.asesoriabancaria.dto.ConsultaDto;
import com.asesoriabancaria.models.Consulta;

public interface ConsultaService {
	void eliminar(int cod);

	List<Consulta> findConsulta();

	Consulta addConsulta(Consulta consulta);

	Consulta updateConsulta(Consulta consulta);

	Consulta findById(int cod);

	List<ConsultaDto> listConsulta();

	List<Consulta> listConsultaxCliente(int codCliente);

	int siguienteCodigo();
}
