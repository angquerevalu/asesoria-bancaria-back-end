package com.asesoriabancaria.service;

import java.util.List;

import com.asesoriabancaria.models.Cliente;

public interface ClienteService {

	public List<Cliente> obtenerClientes();

	public Cliente guardarCliente(Cliente cliente);

	public Cliente login(String email, String pass);

	public Cliente update(Cliente cliente);

	public void eliminarRegistros();
}
