package com.asesoriabancaria.service;

import com.asesoriabancaria.models.Respuesta;

public interface RespuestaService {
	public Respuesta addRespuesta(Respuesta r);

	public Respuesta existeRespuesta(int codConsulta);
}
