package com.asesoriabancaria.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asesoriabancaria.models.Estado;
import com.asesoriabancaria.repository.EstadoRepository;
import com.asesoriabancaria.service.EstadoService;

@Service
public class EstadoServiceImpl implements EstadoService {
	@Autowired
	private EstadoRepository repo;

	@Override
	public List<Estado> obtenerEstados() {
		return repo.findAll();
	}

	@Override
	public Estado addEstado(Estado e) {
		return repo.save(e);
	}

	@Override
	public Estado findById(int cod) {
		return repo.findById(cod).orElse(null);
	}

	@Override
	public void eliminar(int cod) {
		repo.deleteById(cod);
	}

	@Override
	public Estado updateEstado(Estado estado) {
		return repo.save(estado);
	}

	@Override
	public int siguienteCodigo() {
		return repo.siguienteCodigo();
	}

}
