package com.asesoriabancaria.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asesoriabancaria.models.Categoria;
import com.asesoriabancaria.repository.CategoriaRepository;
import com.asesoriabancaria.service.CategoriaService;

@Service
public class CategoriaServiceImpl implements CategoriaService {
	@Autowired
	private CategoriaRepository repo;

	@Override
	public void eliminar(int cod) {
		repo.deleteById(cod);

	}

	@Override
	public List<Categoria> findCategoria() {

		return repo.findAll();
	}

	@Override
	public Categoria addCategoria(Categoria categoria) {
		return repo.save(categoria);
	}

	@Override
	public Categoria updateCategoria(Categoria categoria) {
		return repo.save(categoria);
	}

	@Override
	public Categoria findById(int cod) {

		return repo.findById(cod).orElse(null);
	}

	@Override
	public int siguienteCodigo() {
		return repo.siguienteCodigo();
	}

}
