package com.asesoriabancaria.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asesoriabancaria.models.Cliente;
import com.asesoriabancaria.repository.ClienteRepository;
import com.asesoriabancaria.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	public List<Cliente> obtenerClientes() {
		return clienteRepository.findAll();
	}

	@Override
	public Cliente guardarCliente(Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	@Override
	public Cliente login(String email, String pass) {
		return clienteRepository.login(email, pass);
	}

	@Override
	public void eliminarRegistros() {
		clienteRepository.deleteAll();

	}

	@Override
	public Cliente update(Cliente cliente) {
		return clienteRepository.save(cliente);
	}

}
