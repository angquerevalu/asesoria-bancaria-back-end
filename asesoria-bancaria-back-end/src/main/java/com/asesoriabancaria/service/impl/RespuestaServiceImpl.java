package com.asesoriabancaria.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asesoriabancaria.models.Respuesta;
import com.asesoriabancaria.repository.RespuestaRepository;
import com.asesoriabancaria.service.RespuestaService;

@Service
public class RespuestaServiceImpl implements RespuestaService {

	@Autowired
	RespuestaRepository repo;

	@Override
	public Respuesta addRespuesta(Respuesta r) {
		return repo.save(r);
	}

	@Override
	public Respuesta existeRespuesta(int codConsulta) {
		return repo.respuestaxConsulta(codConsulta);
	}

}
