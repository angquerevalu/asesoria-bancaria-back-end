package com.asesoriabancaria.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asesoriabancaria.dto.ConsultaDto;
import com.asesoriabancaria.models.Consulta;
import com.asesoriabancaria.repository.ConsultaRepository;
import com.asesoriabancaria.service.ConsultaService;

@Service
public class ConsultaServiceImpl implements ConsultaService {
	@Autowired
	private ConsultaRepository consultaRepository;

	@Override
	public List<ConsultaDto> listConsulta() {
		List<ConsultaDto> lstConsulta = new ArrayList<>();
		consultaRepository.listConsulta().forEach(x -> {
			ConsultaDto consultaDto = new ConsultaDto();
			consultaDto.setDesCategoria(String.valueOf(x[0]));
			consultaDto.setDesCategoria(String.valueOf(x[1]));
			consultaDto.setPrecio(Double.valueOf(String.valueOf(x[2])));
			consultaDto.setEstado(String.valueOf(x[3]));
			lstConsulta.add(consultaDto);
		});

		return lstConsulta;
	}

	@Override
	public void eliminar(int cod) {
		consultaRepository.deleteById(cod);

	}

	@Override
	public List<Consulta> findConsulta() {
		return consultaRepository.findAll();
	}

	@Override
	public Consulta addConsulta(Consulta consulta) {
		return consultaRepository.save(consulta);
	}

	@Override
	public Consulta updateConsulta(Consulta consulta) {
		return consultaRepository.save(consulta);
	}

	@Override
	public Consulta findById(int cod) {
		return consultaRepository.findById(cod).orElse(null);
	}

	@Override
	public List<Consulta> listConsultaxCliente(int codCliente) {
		return consultaRepository.listarConsultaxCliente(codCliente);
	}

	@Override
	public int siguienteCodigo() {
		return consultaRepository.siguienteCodigo();
	}

}
