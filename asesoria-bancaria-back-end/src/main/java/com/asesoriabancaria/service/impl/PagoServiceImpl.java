package com.asesoriabancaria.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asesoriabancaria.models.Pago;
import com.asesoriabancaria.repository.PagoRepository;
import com.asesoriabancaria.service.PagoService;

@Service
public class PagoServiceImpl implements PagoService {

	@Autowired
	PagoRepository repo;

	@Override
	public Pago addPago(Pago p) {
		return repo.save(p);
	}

}
